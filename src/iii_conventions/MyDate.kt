package iii_conventions

data class MyDate(val year: Int, val month: Int, val dayOfMonth: Int) {
    operator fun compareTo(other: MyDate): Int {
        val y = year - other.year
        val m = month - other.month
        val d = dayOfMonth - other.dayOfMonth
        return if (y != 0) y else { if (m != 0 ) m else d }
    }
}

operator fun MyDate.rangeTo(other: MyDate): DateRange = DateRange(this, other)

operator fun MyDate.plus(timeInterval: TimeInterval) = addTimeIntervals(timeInterval, 1)
operator fun MyDate.plus(timeIntervals: TimeIntervals) = addTimeIntervals(timeIntervals.timeInterval, timeIntervals.multiple)

enum class TimeInterval {
    DAY,
    WEEK,
    YEAR
}

data class TimeIntervals(val timeInterval: TimeInterval, val multiple: Int)

operator fun TimeInterval.times(multiple: Int) = TimeIntervals(this, multiple)

class DateRange(val start: MyDate, val endInclusive: MyDate): Iterable<MyDate> {
    override fun iterator(): Iterator<MyDate> = object: Iterator<MyDate> {
        var here = start

        override fun hasNext(): Boolean = here <= endInclusive

        override fun next(): MyDate {
            val result = here
            here = result.nextDay()
            return result
        }
    }

    operator fun contains(date: MyDate) = start <= date && date <= endInclusive

}
